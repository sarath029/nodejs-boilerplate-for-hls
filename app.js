const express = require('express');
const app = express();
const path = require('path');

app.set('views',path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.get('*.css', (req, res, next) => {
    req.url = req.url + '.gz';
    console.log(req.url)
    res.set('Content-Encoding', 'gzip');
    res.set('Content-Type', 'text/css; charset=UTF-8');
    next();
});

app.get('*.js', (req, res, next) => {
    req.url = req.url + '.gz';
    console.log(req.url)
    res.set('Content-Encoding', 'gzip');
    res.set('Content-Type', 'application/javascript; charset=UTF-8');
    next();
});

app.get('*.ts', (req, res, next) => {
    req.url = req.url + '.gz';
    console.log(req.url)
    res.set('Content-Encoding', 'gzip');
    res.set('Content-Type', 'application/x-mpegURL; charset=UTF-8');
    next();
});

app.use(express.static('public'))

app.get('/', function(req, res){
    res.render('index',{
        title: 'Nodemon'
    });
})


app.listen(3000, function(){
    console.log('Server started on port 3000');
});